<?php

namespace App\Tests;

use App\Service\DeliveryEstimateCalculator;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

class DeliveryEstimateCalculatorTest extends KernelTestCase
{
    /** @var DeliveryEstimateCalculator $deliveryEstimateCalculator */
    private $deliveryEstimateCalculator;

    protected function setUp()
    {
        static::bootKernel();

        $this->deliveryEstimateCalculator = static::$kernel->getContainer()->get(DeliveryEstimateCalculator::class);
    }

    protected function tearDown() : void
    {
        $this->deliveryEstimateCalculator = null;
    }

    public function mainScenariosShippingProvider(): array
    {
        return [
            ['2020-08-17 00:00:00', 'GB', '2020-08-18'], // Monday - Pre 4PM - UK
            ['2020-08-17 18:00:00', 'GB', '2020-08-19'], // Monday - Post 4PM - UK
            ['2020-08-17 06:26:00', 'NL', '2020-08-20'], // Monday - Pre 4PM - EU
            ['2020-08-17 23:59:59', 'NL', '2020-08-21'], // Monday - Post 4PM - EU
            ['2020-08-17 12:00:00', 'NZ', '2020-08-27'], // Monday - Pre 4PM - World
            ['2020-08-17 18:00:00', 'NZ', '2020-08-28'], // Monday - Post 4PM - World
            ['2020-08-18 15:30:00', 'GB', '2020-08-19'], // Tuesday - Pre 4PM - UK
            ['2020-08-18 18:00:00', 'GB', '2020-08-20'], // Tuesday - Post 4PM - UK
            ['2020-08-18 15:30:00', 'NL', '2020-08-21'], // Tuesday - Pre 4PM - EU
            ['2020-08-18 18:00:00', 'NL', '2020-08-24'], // Tuesday - Post 4PM - EU
            ['2020-08-18 15:30:00', 'NZ', '2020-08-28'], // Tuesday - Pre 4PM - World
            ['2020-08-18 18:00:00', 'NZ', '2020-08-31'], // Tuesday - Post 4PM - World
            ['2020-08-19 15:30:00', 'GB', '2020-08-20'], // Wednesday - Pre 4PM - UK
            ['2020-08-19 18:00:00', 'GB', '2020-08-21'], // Wednesday - Post 4PM - UK
            ['2020-08-19 15:30:00', 'NL', '2020-08-24'], // Wednesday - Pre 4PM - EU
            ['2020-08-19 18:00:00', 'NL', '2020-08-25'], // Wednesday - Post 4PM - EU
            ['2020-08-19 15:30:00', 'NZ', '2020-08-31'], // Wednesday - Pre 4PM - World
            ['2020-08-19 18:00:00', 'NZ', '2020-09-01'], // Wednesday - Post 4PM - World
            ['2020-08-20 15:30:00', 'GB', '2020-08-21'], // Thursday - Pre 4PM - UK
            ['2020-08-20 18:00:00', 'GB', '2020-08-24'], // Thursday - Post 4PM - UK
            ['2020-08-20 15:30:00', 'NL', '2020-08-25'], // Thursday - Pre 4PM - EU
            ['2020-08-20 18:00:00', 'NL', '2020-08-26'], // Thursday - Post 4PM - EU
            ['2020-08-20 15:30:00', 'NZ', '2020-09-01'], // Thursday - Pre 4PM - World
            ['2020-08-20 18:00:00', 'NZ', '2020-09-02'], // Thursday - Post 4PM - World
            ['2020-08-21 15:30:00', 'GB', '2020-08-24'], // Friday - Pre 4PM - UK
            ['2020-08-21 18:00:00', 'GB', '2020-08-25'], // Friday - Post 4PM - UK
            ['2020-08-21 15:30:00', 'NL', '2020-08-26'], // Friday - Pre 4PM - EU
            ['2020-08-21 18:00:00', 'NL', '2020-08-27'], // Friday - Post 4PM - EU
            ['2020-08-21 15:30:00', 'NZ', '2020-09-02'], // Friday - Pre 4PM - World
            ['2020-08-21 18:00:00', 'NZ', '2020-09-03'], // Friday - Post 4PM - World
            ['2020-08-22 15:30:00', 'GB', '2020-08-25'], // Saturday - Pre 4PM - UK
            ['2020-08-22 18:00:00', 'GB', '2020-08-25'], // Saturday - Post 4PM - UK
            ['2020-08-22 15:30:00', 'NL', '2020-08-27'], // Saturday - Pre 4PM - EU
            ['2020-08-22 18:00:00', 'NL', '2020-08-27'], // Saturday - Post 4PM - EU
            ['2020-08-22 15:30:00', 'NZ', '2020-09-03'], // Saturday - Pre 4PM - World
            ['2020-08-22 18:00:00', 'NZ', '2020-09-03'], // Saturday - Post 4PM - World
            ['2020-08-23 15:30:00', 'GB', '2020-08-25'], // Sunday - Pre 4PM - UK
            ['2020-08-23 18:00:00', 'GB', '2020-08-25'], // Sunday - Post 4PM - UK
            ['2020-08-23 15:30:00', 'NL', '2020-08-27'], // Sunday - Pre 4PM - EU
            ['2020-08-23 18:00:00', 'NL', '2020-08-27'], // Sunday - Post 4PM - EU
            ['2020-08-23 15:30:00', 'NZ', '2020-09-03'], // Sunday - Pre 4PM - World
            ['2020-08-23 18:00:00', 'NZ', '2020-09-03'], // Sunday - Post 4PM - World
        ];
    }

    /**
     * @dataProvider mainScenariosShippingProvider
     *
     * @param $orderDateTime
     * @param $countryCode
     * @param $expectedDeliveryDate
     *
     * @throws Exception
     */
    public function testDeliveryIsEstimatedCorrectlyForMainScenarios($orderDateTime, $countryCode, $expectedDeliveryDate): void
    {
        $expectedDeliveryDate = new DateTime($expectedDeliveryDate);

        self::assertEquals($expectedDeliveryDate, $this->deliveryEstimateCalculator->getEstimatedDeliveryDate($orderDateTime, $countryCode, 'TechStore'));
    }

    /**
     * @throws Exception
     */
    public function testInvalidDateParameterThrowsException()
    {
        $this->expectException(HttpException::class);

        $this->deliveryEstimateCalculator->getEstimatedDeliveryDate('2020-13-29 00:00:00', 'GB', 'TechStore');
    }

    /**
     * @throws Exception
     */
    public function testInvalidCountryCodeParameterThrowsException()
    {
        $this->expectException(HttpException::class);

        $this->deliveryEstimateCalculator->getEstimatedDeliveryDate('2020-09-01 00:00:00', 'ZZ', 'TechStore');
    }

    /**
     * @throws Exception
     */
    public function testInvalidSupplierParameterThrowsException()
    {
        $this->expectException(HttpException::class);

        $this->deliveryEstimateCalculator->getEstimatedDeliveryDate('2020-09-01 00:00:00', 'GB', 'TechyyyyStore');
    }
}
