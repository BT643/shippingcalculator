# Delivery Estimate Calculator

Calculate expected delivery time

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Standard symfony requirements as taken from: https://symfony.com/doc/current/setup.html

```
Before creating your first Symfony application you must:

Install PHP 7.2.5 or higher and these PHP extensions (which are installed and enabled by default in most PHP 7 installations): Ctype, iconv, JSON, PCRE, Session, SimpleXML, and Tokenizer;
Install Composer, which is used to install PHP packages.
Optionally, you can also install Symfony CLI. This creates a binary called symfony that provides all the tools you need to develop and run your Symfony application locally.
```


### Installing

Clone this repository to a location of your choice and then:
```
- cd [directory-you-used]
- composer install
- symfony server:start
```
Open the following URL in a browser:
[http://127.0.0.1:8000/api/delivery_estimate/?orderDate=2020-08-15T13:34:00&countryCode=GB&supplier=TechStore](http://127.0.0.1:8000/api/delivery_estimate/?orderDate=2020-08-15T13:34:00&countryCode=GB&supplier=TechStore)

## Running the tests

```
bin/phpunit
```

## Built With

* [Symfony 5.1](https://symfony.com/) - The web framework used
* [Composer](https://getcomposer.org/) - Dependency Management

## Authors

* **Ben Thomas**
