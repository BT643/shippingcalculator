<?php

namespace App\Controller;

use App\Service\DeliveryEstimateCalculator;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route("/api")
 */
class DeliveryEstimateController extends AbstractFOSRestController
{
    /**
     * @Rest\View()
     * @Rest\Get("/delivery_estimate/")
     *
     * @Rest\QueryParam(
     *     name="orderDate",
     *     nullable=false,
     *     allowBlank=false,
     *     description="Order date and time.",
     *     strict=true
     * )
     * @Rest\QueryParam(
     *     name="countryCode",
     *     nullable=false,
     *     allowBlank=false,
     *     description="Country code for the order.",
     *     strict=true
     * )
     * @Rest\QueryParam(
     *     name="supplier",
     *     nullable=false,
     *     allowBlank=false,
     *     description="Supplier for the order.",
     *     strict=true
     * )
     * @Rest\QueryParam(
     *     name="courier",
     *     nullable=true,
     *     allowBlank=true,
     *     default="RoyalMail",
     *     description="Courier for the shipment.",
     *     strict=true
     * )
     *
     * @param DeliveryEstimateCalculator $deliveryEstimateCalculator
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return View
     *
     * @throws Exception
     */
    public function getDeliveryEstimate(DeliveryEstimateCalculator $deliveryEstimateCalculator, ParamFetcherInterface $paramFetcher): View
    {
        $countryCode = $paramFetcher->get('countryCode');
        $supplier = $paramFetcher->get('supplier');
        $courier = $paramFetcher->get('courier');
        $orderDateTime = $paramFetcher->get('orderDate');

        $estimatedDeliveryDate = $deliveryEstimateCalculator->getEstimatedDeliveryDate($orderDateTime, $countryCode, $supplier, $courier);

        return View::create(
            [
                'status' => 'success',
                'data' => [
                    'estimatedDeliveryDate' => $estimatedDeliveryDate->format('Y-m-d')
                ],
                'message' => null
            ],
            Response::HTTP_OK
        );
    }
}
