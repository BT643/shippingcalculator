<?php


namespace App\Service;

use DateTime;

interface CourierInterface
{
    public function calculateDelivery(DateTime $shippedAt, string $deliveryCountryCode): DateTime;
}
