<?php


namespace App\Service;


trait EuChecker
{
    /**
     * @param $countryCode
     *
     * @return bool
     */
    public function isEU(string $countryCode): bool {
        $euCountryCodes = [
            'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL',
            'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',
            'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'
        ];

        return in_array($countryCode, $euCountryCodes, true);
    }
}
