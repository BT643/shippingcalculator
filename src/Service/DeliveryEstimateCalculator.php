<?php

namespace App\Service;

use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Constraints\Country;
use Symfony\Component\Validator\Validation;

class DeliveryEstimateCalculator
{
    private $courierFactory;

    private $supplierFactory;

    /**
     * @param CourierFactory $courierFactory
     * @param SupplierFactory $supplierFactory
     */
    public function __construct(CourierFactory $courierFactory, SupplierFactory $supplierFactory)
    {
        $this->supplierFactory = $supplierFactory;
        $this->courierFactory = $courierFactory;
    }

    /**
     * @param string $orderDate
     * @param string $countryCode
     * @param string $supplier
     * @param string $courier
     *
     * @return DateTime
     *
     * @throws \Exception
     */
    public function getEstimatedDeliveryDate(string $orderDate, string $countryCode, string $supplier, string $courier = 'RoyalMail'): DateTime
    {
        $orderDateTime = $this->validateDateParameter($orderDate);

        $this->validateCountryCodeParameter($countryCode);

        // Get the dispatch date from the supplier
        $supplierClass = $this->supplierFactory->get($supplier);
        $dispatchDate = $supplierClass->getDispatchDateBasedOnOrderDate($orderDateTime);

        //And use that dispatch date with the supplier, country, and courier
        $courierClass = $this->courierFactory->get($courier);

        return $courierClass->calculateDelivery($dispatchDate, $countryCode);
    }

    /**
     * @param string $countryCode
     *
     * @return bool
     */
    private function isCountryCodeValid(string $countryCode): bool
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate($countryCode, [
            new Country(),
        ]);

        return $violations->count() === 0;
    }

    /**
     * @param string $orderDate
     *
     * @return DateTime
     */
    private function validateDateParameter(string $orderDate): DateTime
    {
        try {
            $orderDateTime = new DateTime($orderDate);
        } catch (Exception $exception) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Order date is not valid.');
        }

        return $orderDateTime;
    }

    /**
     * @param string $countryCode
     */
    private function validateCountryCodeParameter(string $countryCode): void
    {
        if (false === $this->isCountryCodeValid($countryCode)) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Country code is invalid.');
        }
    }
}
