<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SupplierFactory
{
    public function get(string $type)
    {
        switch ($type) {
            case 'TechStore':
                return new TechStore();
                break;
            default:
                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Supplier not found.');
        }
    }
}
