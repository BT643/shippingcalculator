<?php


namespace App\Service;

use DateTime;

interface SupplierInterface
{
    public function getDispatchDateBasedOnOrderDate(DateTime $orderDateTime): DateTime;
}
