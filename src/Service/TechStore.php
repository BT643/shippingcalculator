<?php

namespace App\Service;

use DateTime;

class TechStore implements SupplierInterface
{
    // Monday to Friday
    private $dispatchDays = ['1', '2', '3', '4', '5'];

    // 24 hour time - hour element
    private $orderBeforeTimeForSameDayDispatch = 15;

    /**
     * @param DateTime $orderDateTime
     * @return DateTime
     *
     * @throws \Exception
     */
    public function getDispatchDateBasedOnOrderDate(DateTime $orderDateTime): DateTime
    {
        if ($this->orderDayIsWeekday($orderDateTime)) {
            // Before x time gets same day dispatch
            return $this->orderTimeBeforeSameDayCutoff($orderDateTime) ? $orderDateTime : $this->getNextWorkingDay($orderDateTime);
        }

        return $this->getNextWorkingDay($orderDateTime);
    }

    /**
     * @param DateTime $orderDateTime
     *
     * @return bool
     */
    private function orderTimeBeforeSameDayCutoff(DateTime $orderDateTime): bool
    {
        return date('G', $orderDateTime->getTimestamp()) <= $this->orderBeforeTimeForSameDayDispatch;
    }

    /**
     * @param DateTime $orderDateTime
     *
     * @return bool
     */
    private function orderDayIsWeekday(DateTime $orderDateTime): bool
    {
        return in_array(date('w', $orderDateTime->getTimestamp()), $this->getDispatchDays(), true);
    }

    /**
     * @return array
     */
    private function getDispatchDays(): array
    {
        return $this->dispatchDays;
    }

    /**
     * @param DateTime $orderDateTime
     * @return DateTime
     *
     * @throws \Exception
     */
    private function getNextWorkingDay(DateTime $orderDateTime)
    {
        // Set dispatch date to next working day
        $dispatchDate = date('Y-m-d', strtotime('+1 weekdays', $orderDateTime->getTimestamp()));

        return new DateTime($dispatchDate);
    }
}
