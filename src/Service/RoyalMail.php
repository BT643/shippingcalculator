<?php

namespace App\Service;

use DateTime;

class RoyalMail implements CourierInterface
{
    use EuChecker;

    private const UK_LEAD_TIME = 1;
    private const EU_LEAD_TIME = 3;
    private const REST_OF_THE_WORLD_LEAD_TIME = 8;

    /**
     * @param DateTime $shippedAt
     * @param string $deliveryCountryCode
     *
     * @return DateTime
     *
     * @throws \Exception
     */
    public function calculateDelivery(DateTime $shippedAt, string $deliveryCountryCode): DateTime
    {
        $addWeekdaysString = sprintf('+%d weekdays', $this->getLeadTimeForCountry($deliveryCountryCode));

        $deliveryDate = date('Y-m-d', strtotime($addWeekdaysString, $shippedAt->getTimestamp()));

        return new DateTime($deliveryDate);
    }

    /**
     * @param string $deliveryCountryCode
     *
     * @return int
     */
    private function getLeadTimeForCountry(string $deliveryCountryCode): int
    {
        if ($deliveryCountryCode === 'GB') {
            return self::UK_LEAD_TIME;
        }

        if ($this->isEU($deliveryCountryCode)) {
            return self::EU_LEAD_TIME;
        }

        return self::REST_OF_THE_WORLD_LEAD_TIME;
    }
}
