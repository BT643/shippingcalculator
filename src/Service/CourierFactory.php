<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CourierFactory
{
    public function get($courier)
    {
        switch ($courier) {
            case 'RoyalMail':
                return new RoyalMail();
                break;
            default:
                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'Courier not found.');
        }
    }
}
